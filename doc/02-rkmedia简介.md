[返回README](../README.md)

本文讲解瑞星微的一个多媒体框架——rkmedia和一个图像处理硬件加速模块——rga。
本文将介绍其中的一些概念，同时结合代码分析其如何使用。
官方也有文档，存放在doc/ref目录下，它们在sdk中的路径分别为:

- rkmedia: ${sdk_root}/docs/RV1126_RV1109/Multimedia/Rockchip_Developer_Guide_Linux_RKMedia_CN.pdf
- rga: ${sdk_root}/external/linux-rga/docs/RGA_API_Instruction.md

# 1. 初识rkmedia

他们整体的系统框图如下:

![1-1.png](pic/ch2/1-1.png)

>PS: 音频相关的东西我们可以不关注

## 1.1 图像格式的若干说明

在图像处理时，我们会将图像看成3个矩阵，每个矩阵的大小对应图像分辨率。3个矩阵表示rgb三种颜色
但是，在摄像头实际处理的过程中并不完全是这样，人们对应摄像头的数据格式由诸多定义，但主要分为几类:

- rgb格式: 我们熟知的格式，每个像素用 红 绿 蓝来表示
- argb格式: 在rgb的基础上添加了alpha通道，用于描述透明度，多在显示器端用
- yuv格式: 另一中编码格式,y表示明亮度，uv表示色度

其实主要就两类: rgb和yuv 它们之间有固定的转换公式:
- R = Y + 1.4075 * V;  
- G = Y - 0.3455 * U - 0.7169*V;  
- B = Y + 1.779 * U; 

>PS: 转换公式不唯一，根据标准而定，详见[YUV与RGB互转各种公式](https://www.cnblogs.com/luoyinjie/p/7219319.html)

实际过程中，yuv数据用的较多。**那么，为什么非要用这么奇怪的编码方式呢？**

答案是**压缩数据量**。

人眼对亮度的感知更加明显，而对色彩就相对薄弱（看看杆状细胞和锥状细胞的数量对比就能看出了）.
因此，使用yuv格式时，可以通过减少色度信息来压缩数据量而不会对成像效果有过多影响。
我们常用的格式就是NV12，又称YUV420，它的uv数据只有y数据的1/4，
相较于rgb格式，数据量减少了一半

值得注意的是，yuv格式的图像有所不同。主要有两种方式:

- 平面格式: 先存储Y数据，然后存储U数据，最后是V数据
- 打包格式: 每个像素点的YUV分量交替连续存储

举个栗子: 
- rgb图像的存储格式为:rgb, rgb, ..., rgb
- NV12存储格式为:yy....y, uu...u, vv...v
- YUYV存储格式为:yuyv, yuyv, ...., yuyv

更多格式说明可从: [YUV pixel formats](https://fourcc.org/yuv.php)查看

>PS: 
> 1. 现阶段，主要关注nv12格式和rgb888格式即可
> 2. rgb也是可以压缩的，比如rgb565 ^v^

## 1.2 rkmedia的主要模块

rkmedia是一套庞大的API，分为若干个模块，我们只需要了解以下几个模块即可:

- vi 视频输入，从摄像头获取数据
- venc 视频编码,压缩并推流时候用到
- vo 视频输出，讲视频输出到摄像头
- rga 图像处理子模块 进行图像的裁切，缩放，格式转换等

>PS: rkmedia的rga模块和rga功能类似，但rga那套api功能更全

## 1.3 rkmedia数据链路

我们写代码的目的就是将1.2中的几个模块连接起来，根据需求，主要有两种链路

1. 显示器输出: vi -> (rga) -> vo
2. PC推流查看: vi -> (rga) -> venc

将两个模块链接起来的方式有两种:

1. 使用rkmedia自带的bind函数: RK_MPI_SYS_Bind
2. 从输入模块获取buffer，然后给输出模块输送buffer
涉及的函数有: RK_MPI_SYS_GetMediaBuffer 和 RK_MPI_SYS_SendMediaBuffer

rkmeida的demo中大多使用第1种方式来链接，该方式简单且延时较低，
追求快速实现和低延时可使用；
第2种方式虽然更加繁琐，但可以获得图像数据，并对其进行处理，添加算法时最好谁用该方式。
本文的讲解主要基于rkmedia官方demo，因此只讲解第一种链接方式

# 2. rkmedia_vi_vo_test.c讲解

针对第一种显示链路,即vi->vo,我们结合瑞星微自己的demo来进行分析

## 2.1 参数解析器(后面多半用不到,可跳过)

这部分的代码比较散,主要分布在3个地方

代码段1
``` c
// line 26
static RK_CHAR optstr[] = "?::a::I:M:";
static const struct option long_options[] = {
    {"aiq", optional_argument, NULL, 'a'},
    {"camid", required_argument, NULL, 'I'},
    {"multictx", required_argument, NULL, 'M'},
    {"help", optional_argument, NULL, '?'},
    {NULL, 0, NULL, 0},
};
static RK_CHAR optstr[] = "?::a::I:M:";
static const struct option long_options[] = {
    {"aiq", optional_argument, NULL, 'a'},
    {"camid", required_argument, NULL, 'I'},
    {"multictx", required_argument, NULL, 'M'},
    {"help", optional_argument, NULL, '?'},
    {NULL, 0, NULL, 0},
};
```

代码段2
```c
// line 34
static void print_usage(const RK_CHAR *name) {
  printf("usage example:\n");
#ifdef RKAIQ
  printf("\t%s [-a [iqfiles_dir]]"
         "[-I 0] "
         "[-M 0] "
         "\n",
         name);
  printf("\t-a | --aiq: enable aiq with dirpath provided, eg:-a "
         "/oem/etc/iqfiles/, "
         "set dirpath empty to using path by default, without this option aiq "
         "should run in other application\n");
  printf("\t-M | --multictx: switch of multictx in isp, set 0 to disable, set "
         "1 to enable. Default: 0\n");
#else
  printf("\t%s [-I 0]\n", name);
#endif
  printf("\t-I | --camid: camera ctx id, Default 0\n");
}
```

代码段3
```c
// line 64
  int c;
  char *iq_file_dir = NULL;
  while ((c = getopt_long(argc, argv, optstr, long_options, NULL)) != -1) {
    const char *tmp_optarg = optarg;
    switch (c) {
    case 'a':
      if (!optarg && NULL != argv[optind] && '-' != argv[optind][0]) {
        tmp_optarg = argv[optind++];
      }
      if (tmp_optarg) {
        iq_file_dir = (char *)tmp_optarg;
      } else {
        iq_file_dir = "/oem/etc/iqfiles";
      }
      break;
    case 'I':
      s32CamId = atoi(optarg);
      break;
#ifdef RKAIQ
    case 'M':
      if (atoi(optarg)) {
        bMultictx = RK_TRUE;
      }
      break;
#endif
    case '?':
    default:
      print_usage(argv[0]);
      return 0;
    }
  }
```

这三段代码的作用主要是解析用户输入的命令行参数

用户执行程序的时候,可以通过命令行将一些值传递给程序,从而实现从外部控制程序,
如果要使用命令行参数,mian函数的形式一般如下:
```c
    int main(int argc, char*argv[] )
```
其中,argc指传入参数数量,argv则是一个指针数组,指向每个传入的参数

下面是rkmedia_vi_vo_test的执行命令,可以借此来理解一下命令行参数

```shell
rkmedia_vi_vo_test -a /etc/iqfiles
```

我们可以看到,传入了两个命令行参数 "-a" 和 "/etc/iqfiles"

本代码中对命令行的解析主要在代码3,他的核心主要是getopt_long,函数原型如下:

```c
int getopt_long(int argc, char * const argv[], const char *optstring,const struct option *longopts, int *longindex);
```
argc和argv与main函数的两个参数一致,
而optstring和longopts则在代码段1中赋值

optstring表示短选项字符串,他们的功能区分主要依靠":",其含义为:

|情况|说明|案例|
|---|---|---|
|单字符,不带冒号   |只表示选项|-a|
|单字符,带一个冒号 |表示选项后带一个参数|-a 100|
|单字符,带两个冒号 |表示选项后带一个可选参数,若带参数则选项与参数不能有空格|-a100|

比如本例中的字符串可进行如下拆分:
"?::a::I:M:" = "?::" + "a::" + "I:" + "M:"

因此,可以推测出下面这样的输入案例:
```shell
rkmedia_vi_vo_test -a/etc/iqfiles -I 1 -M 0
```

>PS: 按理说,a:: 表示参数得跟选项靠在一起,但实际测试时发现,加空格也无所谓

longopts则表示长选项结构体,其原型如下:
```c
struct option 
{  
     const char *name;      ///< 选项名称
     int         has_arg;   ///< 是否携带参数
     int        *flag;      ///< 空或非空
     int         val;       ///< 指定函数找到该选项的返回值
}; 
```

其中name和val含义显而易见,主要讲讲has_arg和flag

has_arg有三个不同的值:
|值|含义|案例|
|no_argument|参数后不跟参数值|--version|
|required_argument|输入格式为 --参数 值 或 --参数=值|--a=100 --a 100|
|optional_argument|输入格式只能为--参数=值|--a=100|

下面回到代码3,其逻辑就是通过while循环获取输入的参数,
调用case语句处理不同的选项,设定不同的变量值,
如果输入了未知参数,则打印用法(见代码段2)

case语句中有两个东西可能会令人疑惑,atoi函数和optind变量.
optind变量是系统自定义的,每次调用getopt_long时会+1
而atoi则是将字符串转为整数

>PS: 俺也有点懵,不会用就别硬用了,还是老老实实的定死变量吧QAQ

## 2.2 异常信号捕获(可跳过)

这部分代码分为两部分,都很短(不是逐行看都不一定能注意到o_0)

代码段1
```c
// line 19
static bool quit = false;
static void sigterm_handler(int sig) {
  fprintf(stderr, "signal %d\n", sig);
  quit = true;
}
```
代码段2
```c
//  line 207
signal(SIGINT, sigterm_handler);
while (!quit) {
        usleep(500000);
}
```

这部分代码核心是signal函数,其原型如下:

```c
void (*signal(int sig, void (*func)(int)))(int)
```

他的作用是给一个信号量(sig)注册一个回调函数,
回调函数较为复杂,暂时理解成遇到这个信号量就执行这个函数就行,
对于本段代码,就是sigterm_handler函数(代码段1中)

信号量有下面几种

|宏|信号|
|SIGABRT|程序异常终止|
|SIGFPE|算术运算出错,若除0|
|SIGILL|非法函数影像,如非法指令|
|SIGINT|中断信号,如用户输入的ctrl+c|
|SIGSEGV|非法访问存储器,如访问不存在的内存单元|
|SIGTREM|发送给本程序的终止请求信号|

本段代码中,我们用的是SIGINT信号量,当输入中断信号后，
下面的while循环就会结束，开始回收资源

## 2.2 ISP初始化

代码段:
```c
// line 97
if (iq_file_dir) {
#ifdef RKAIQ
    printf("#Rkaiq XML DirPath: %s\n", iq_file_dir);
    printf("#bMultictx: %d\n\n", bMultictx);
    rk_aiq_working_mode_t hdr_mode = RK_AIQ_WORKING_MODE_NORMAL;
    int fps = 30;
    SAMPLE_COMM_ISP_Init(s32CamId, hdr_mode, bMultictx, iq_file_dir);
    SAMPLE_COMM_ISP_Run(s32CamId);
    SAMPLE_COMM_ISP_SetFrameRate(s32CamId, fps);
#endif
  }
```

这部分代码可以理解成固定用法，主要干了下面几件事:
1. 设置工作模式，线性模式
2. 初始化isp
3. 启动isp,设置帧率

## 2.4 rkmedia模块的配置

### 2.4.1 系统初始化

代码段
```c
// line 109
  RK_MPI_SYS_Init();
```

在执行所有模块前,需要调用该函数用于初始化rkmedia系统

### 2.4.2 vi初始化

代码段:
```c
//line 110
VI_CHN_ATTR_S vi_chn_attr;
// video设备节点路径
vi_chn_attr.pcVideoNode = "rkispp_scale0";
// 缓冲区数量
vi_chn_attr.u32BufCnt = 3;
// 宽
vi_chn_attr.u32Width = video_width;
// 高
vi_chn_attr.u32Height = video_height;
// 图像格式
vi_chn_attr.enPixFmt = IMAGE_TYPE_NV12;
// VI通道工作模式
vi_chn_attr.enWorkMode = VI_WORK_MODE_NORMAL;
ret = RK_MPI_VI_SetChnAttr(s32CamId, 0, &vi_chn_attr);
ret |= RK_MPI_VI_EnableChn(s32CamId, 0);
if (ret) {
        printf("Create vi[0] failed! ret=%d\n", ret);
        return -1;
}
```

观察上述代码我们不难发现,基本流程如下:
1. 创建结构体并配置
2. 设置结构体并使能通道

上述代码中还有个奇怪的变量,ret,这一般称为错误码,
当程序正常运行时,ret=0,否则就是错误码,vi部分的错误码可以通过文档查看

### 2.4.2 rgn初始化(用不到,只是贴个代码)

代码段
```c
// line 124
  /* test rgn cover */
  COVER_INFO_S CoverInfo;
  OSD_REGION_INFO_S RngInfo;
  memset(&CoverInfo, 0, sizeof(CoverInfo));
  memset(&RngInfo, 0, sizeof(RngInfo));
  CoverInfo.enPixelFormat = PIXEL_FORMAT_ARGB_8888;
  CoverInfo.u32Color = 0xFFFF0000; // blue
  RngInfo.enRegionId = REGION_ID_0;
  RngInfo.u32PosX = 0;
  RngInfo.u32PosY = 0;
  RngInfo.u32Width = 100;
  RngInfo.u32Height = 100;
  RngInfo.u8Enable = 1;
  RK_MPI_VI_RGN_SetCover(s32CamId, 1, &RngInfo, &CoverInfo);
```

### 2.4.3 rga初始化

代码段:
```c
  // rga0 for primary plane
  RGA_ATTR_S stRgaAttr;
  memset(&stRgaAttr, 0, sizeof(stRgaAttr));
  // 使能缓冲池
  stRgaAttr.bEnBufPool = RK_TRUE;
  // 缓冲池数量
  stRgaAttr.u16BufPoolCnt = 3;
  // 旋转角度
  stRgaAttr.u16Rotaion = 90;
  // 输入图像信息
        // 起始x
  stRgaAttr.stImgIn.u32X = 0;
        // 起始y
  stRgaAttr.stImgIn.u32Y = 0;
        // 图像格式
  stRgaAttr.stImgIn.imgType = IMAGE_TYPE_NV12;
        // 宽
  stRgaAttr.stImgIn.u32Width = video_width;
        // 高
  stRgaAttr.stImgIn.u32Height = video_height;
        // 虚宽
  stRgaAttr.stImgIn.u32HorStride = video_width;
        // 虚高
  stRgaAttr.stImgIn.u32VirStride = video_height;
  // 输出图像信息
  stRgaAttr.stImgOut.u32X = 0;
  stRgaAttr.stImgOut.u32Y = 0;
  stRgaAttr.stImgOut.imgType = IMAGE_TYPE_RGB888;
  stRgaAttr.stImgOut.u32Width = disp_width;
  stRgaAttr.stImgOut.u32Height = disp_height;
  stRgaAttr.stImgOut.u32HorStride = disp_width;
  stRgaAttr.stImgOut.u32VirStride = disp_height;
  ret = RK_MPI_RGA_CreateChn(0, &stRgaAttr);
  if (ret) {
    printf("Create rga[0] falied! ret=%d\n", ret);
    return -1;
  }
```

RGA模块的创建流程与vi类似,不过多赘述

我们结合rga的任务就能发现,配置好输入输出图像信息,rga模块就会自行进行
裁切,缩放,格式转换功能,不需要我们取额外配置,但实际测试下来延时较高,
可能需要直接调用rga的函数进行处理

对于图像信息中的变量,可能会对宽高和"虚宽虚高"的概念有点迷惑,
对此,RGA的文档是这么解释的:
- 实宽,实高: 实际操作的图像区域
- 虚宽,虚高: 图像本身的大小

我们一般将他们看成同一个东西即可

>PS: 图像信息的x,y,w,h最好2对齐,即偶数,否则可能会报错

### 2.4.4 vo初始化

代码段:

```c
// line 165
VO_CHN_ATTR_S stVoAttr = {0};
// VO[0] for primary plane
// 视频输出设备节点
stVoAttr.pcDevNode = "/dev/dri/card0";
// 图层类型
stVoAttr.emPlaneType = VO_PLANE_PRIMARY;
// 图像格式
stVoAttr.enImgType = IMAGE_TYPE_RGB888;
// z轴高度
stVoAttr.u16Zpos = 0;
// 起始x
stVoAttr.stDispRect.s32X = 0;
// 起始y
stVoAttr.stDispRect.s32Y = 0;
// 宽
stVoAttr.stDispRect.u32Width = disp_width;
// 高
stVoAttr.stDispRect.u32Height = disp_height;
ret = RK_MPI_VO_CreateChn(0, &stVoAttr);
if (ret) {
        printf("Create vo[0] failed! ret=%d\n", ret);
return -1;
}
```

vo创建流程与vi类似,x,y,w,h的定义与rga中的类似,因此不过多赘述

可能有点迷惑的是图层类型和z轴高度两个变量

显示屏上面一般不会只显示一个画面,通常会涉及到画面的叠加
比如我们在使用电脑打开文件夹时,至少会有下面几个东西: 桌面,文件夹页面,鼠标等
他们三者其实是放在不同的图层中的,我们看到的画面实际是叠加后的结果.
因此我们需要告诉显示器每个图层的顺序,这样才能确定叠加后的效果.
还是以上面电脑的例子来说明,显然桌面在最底下,然后文件夹页面在桌面上面,
鼠标又在文件夹上方.图层的"上下"就是z轴高度,z轴高度越高,画面越在顶层

图层类型则是根据不同图层发挥的作用进行归类的,主要包含下面几种:
|图层类型|说明|
|primary|主要图层,显示背景或图像内容|
|overlay|用于显示叠加,缩放|
|cursor|用于显示鼠标等控件|

>PS: NV12格式只能出现在overlay图层

## 2.5 模块绑定

代码段1:
```c
//line 181
MPP_CHN_S stSrcChn = {0};
MPP_CHN_S stDestChn = {0};

printf("#Bind VI[0] to RGA[0]....\n");
stSrcChn.enModId = RK_ID_VI;
stSrcChn.s32ChnId = 0;
stDestChn.enModId = RK_ID_RGA;
stDestChn.s32ChnId = 0;
ret = RK_MPI_SYS_Bind(&stSrcChn, &stDestChn);
if (ret) {
        printf("Bind vi[0] to rga[0] failed! ret=%d\n", ret);
        return -1;
}
```

代码段2:
```c
// line 195
printf("# Bind RGA[0] to VO[0]....\n");
stSrcChn.enModId = RK_ID_RGA;
stSrcChn.s32ChnId = 0;
stDestChn.enModId = RK_ID_VO;
stDestChn.s32ChnId = 0;
ret = RK_MPI_SYS_Bind(&stSrcChn, &stDestChn);
if (ret) {
        printf("Bind rga[0] to vo[0] failed! ret=%d\n", ret);
        return -1;
}
```

模块绑定主要使用的是RK_MPI_SYS_Bind函数，
主要是指定输入和输出模块的模块类型和id

## 2.6 资源回收

代码段:
```c
// line 212
printf("%s exit!\n", __func__);
  stSrcChn.enModId = RK_ID_VI;
  stSrcChn.s32ChnId = 0;
  stDestChn.enModId = RK_ID_RGA;
  stDestChn.s32ChnId = 0;
  ret = RK_MPI_SYS_UnBind(&stSrcChn, &stDestChn);
  if (ret) {
    printf("UnBind vi[0] to rga[0] failed! ret=%d\n", ret);
    return -1;
  }

  stSrcChn.enModId = RK_ID_RGA;
  stSrcChn.s32ChnId = 0;
  stDestChn.enModId = RK_ID_VO;
  stDestChn.s32ChnId = 0;
  ret = RK_MPI_SYS_UnBind(&stSrcChn, &stDestChn);
  if (ret) {
    printf("UnBind rga[0] to vo[0] failed! ret=%d\n", ret);
    return -1;
  }

  RK_MPI_VO_DestroyChn(0);
  RK_MPI_RGA_DestroyChn(0);
  RK_MPI_VI_DisableChn(s32CamId, 0);

  if (iq_file_dir) {
#ifdef RKAIQ
    SAMPLE_COMM_ISP_Stop(s32CamId);
#endif
  }
  return 0;
```

这部分代码也比较简单，主要涉及下面几个方面:
1. 模块解绑
2. vi vo rga模块销毁
3. 停止isp



# 3. rkmedia_rga_crop_venc_test.c 讲解

本代码有很多与rkmedia_vi_vo_test.c代码重复的部分
汇总如下,请结合第二章自行理解

|说明|代码位置|
|参数解析器   | line 143-157, line 159-204, line 223-277|
|异常信号捕获 | line 40-43, line 294, line 367-369|
|系统初始化   | line 308|
|vi配置      | line 316-329|

值得注意的是，还有下面这段代码跟vi相关
```c
//line 359
usleep(1000); // waite for thread ready.
ret = RK_MPI_VI_StartStream(g_stViChn.s32DevId, g_stViChn.s32ChnId);
if (ret) {
        printf("ERROR: Start Vi[0] failed! ret=%d\n", ret);
        return -1;
}
```
这是因为本代码没有走绑定的方式，而是使用get+send的方式，因此vi需要手动开启数据流


## 3.1 venc配置

代码段
```c
// line 331
VENC_CHN_ATTR_S venc_chn_attr;
memset(&venc_chn_attr, 0, sizeof(venc_chn_attr));
// 编码器属性
        // 编码协议
venc_chn_attr.stVencAttr.enType = RK_CODEC_TYPE_H264;
        // 图像类型
venc_chn_attr.stVencAttr.imageType = g_enPixFmt;
        // 宽
venc_chn_attr.stVencAttr.u32PicWidth = demo_arg.vi_width;
        // 高
venc_chn_attr.stVencAttr.u32PicHeight = demo_arg.vi_height;
        // 虚宽
venc_chn_attr.stVencAttr.u32VirWidth = demo_arg.vi_width;
        // 虚高
venc_chn_attr.stVencAttr.u32VirHeight = demo_arg.vi_height;
        // 编码等级
venc_chn_attr.stVencAttr.u32Profile = 77;
        // 旋转角度
venc_chn_attr.stVencAttr.enRotation = S32Rotation;

// 码率控制器属性
        // 编码协议
venc_chn_attr.stRcAttr.enRcMode = VENC_RC_MODE_H264CBR;
// H264 cbr 模式属性
        // gop
venc_chn_attr.stRcAttr.stH264Cbr.u32Gop = 30;
        // 比特率
venc_chn_attr.stRcAttr.stH264Cbr.u32BitRate =
demo_arg.vi_width * demo_arg.vi_height * 30 / 14;
        // 输出帧率
venc_chn_attr.stRcAttr.stH264Cbr.fr32DstFrameRateDen = 0;
venc_chn_attr.stRcAttr.stH264Cbr.fr32DstFrameRateNum = 30;
        // 输入帧率
venc_chn_attr.stRcAttr.stH264Cbr.u32SrcFrameRateDen = 0;
venc_chn_attr.stRcAttr.stH264Cbr.u32SrcFrameRateNum = 30;

RK_MPI_VENC_CreateChn(g_stVencChn.s32ChnId, &venc_chn_attr);
```

venc创建跟vi类似，也是配置结构体然后创建通道


将venc理解成压缩图像数据的模块,使用默认模块即可,
暂时不用特意去调整参数（固定套路，直接搬运即可）

## 3.2 多线程的创建与结束

代码段1:
```c
// line 354
pthread_t read_thread;
pthread_create(&read_thread, NULL, GetMediaBuffer, &demo_arg);
pthread_t venc_thread;
pthread_create(&venc_thread, NULL, GetVencBuffer, NULL);
```

代码段2
```c
// line 371
printf("%s exit!\n", __func__);
pthread_join(read_thread, NULL);
pthread_join(venc_thread, NULL);
```


多线程简单理解成让一个程序中的多个功能可以同时进行即可，每个功能对应的就是一个线程函数.
本代码有两个线程，对应就是两个线程函数:GetVencBuffer和GetMediaBuffer

多线程的创建见代码段1，其函数原型为:
```c
int pthread_create(pthread_t *tidp,const pthread_attr_t *attr, 
                   (void*)(*start_rtn)(void*),void *arg);
```

- tidp 线程标识符号
- attr 线程选项（可忽略）
- start_rtn 线程函数
- arg 需要传递给线程函数的参数

代码段2的pthread_join则是等待线程结束的指令，只有当线程结束了才会继续往下执行

## 3.3 线程函数



### 3.3.1 GetMediaBuffer线程函数

代码段
```c
// line 76
static void *GetMediaBuffer(void *arg) {
  printf("#Start %s thread, arg:%p\n", __func__, arg);
  // rga初始化
  rga_demo_arg_t *crop_arg = (rga_demo_arg_t *)arg;
  int ret;
  rga_buffer_t src;
  rga_buffer_t dst;
  MEDIA_BUFFER src_mb = NULL;
  MEDIA_BUFFER dst_mb = NULL;
  printf("test, %d, %d, %d, %d\n", crop_arg->target_height,
         crop_arg->target_width, crop_arg->vi_height, crop_arg->vi_width);
  // 开始循环，获取buffer->rga处理->送给venc
  while (!quit) {
    src_mb =
        RK_MPI_SYS_GetMediaBuffer(g_stViChn.enModId, g_stViChn.s32ChnId, -1);
    if (!src_mb) {
      printf("ERROR: RK_MPI_SYS_GetMediaBuffer get null buffer!\n");
      break;
    }

    MB_IMAGE_INFO_S stImageInfo = {
        crop_arg->target_width, crop_arg->target_height, crop_arg->target_width,
        crop_arg->target_height, IMAGE_TYPE_NV12};
    dst_mb = RK_MPI_MB_CreateImageBuffer(&stImageInfo, RK_TRUE, 0);
    RK_MPI_MB_SetTimestamp(dst_mb, RK_MPI_MB_GetTimestamp(src_mb));
    if (!dst_mb) {
      printf("ERROR: RK_MPI_MB_CreateImageBuffer get null buffer!\n");
      break;
    }

    src = wrapbuffer_fd(RK_MPI_MB_GetFD(src_mb), crop_arg->vi_width,
                        crop_arg->vi_height, RK_FORMAT_YCbCr_420_SP);
    dst = wrapbuffer_fd(RK_MPI_MB_GetFD(dst_mb), crop_arg->target_width,
                        crop_arg->target_height, RK_FORMAT_YCbCr_420_SP);
    im_rect src_rect = {crop_arg->target_x, crop_arg->target_y,
                        crop_arg->target_width, crop_arg->target_height};
    im_rect dst_rect = {0};
    ret = imcheck(src, dst, src_rect, dst_rect, IM_CROP);
    if (IM_STATUS_NOERROR != ret) {
      printf("%d, check error! %s", __LINE__, imStrError((IM_STATUS)ret));
      break;
    }
    IM_STATUS STATUS = imcrop(src, dst, src_rect);
    if (STATUS != IM_STATUS_SUCCESS)
      printf("imcrop failed: %s\n", imStrError(STATUS));

    VENC_RESOLUTION_PARAM_S stResolution;
    stResolution.u32Width = crop_arg->target_width;
    stResolution.u32Height = crop_arg->target_height;
    stResolution.u32VirWidth = crop_arg->target_width;
    stResolution.u32VirHeight = crop_arg->target_height;

    RK_MPI_VENC_SetResolution(g_stVencChn.s32ChnId, stResolution);
    RK_MPI_SYS_SendMediaBuffer(g_stVencChn.enModId, g_stVencChn.s32ChnId,
                               dst_mb);
    RK_MPI_MB_ReleaseBuffer(src_mb);
    RK_MPI_MB_ReleaseBuffer(dst_mb);
    src_mb = NULL;
    dst_mb = NULL;
  }

  if (src_mb)
    RK_MPI_MB_ReleaseBuffer(src_mb);
  if (dst_mb)
    RK_MPI_MB_ReleaseBuffer(dst_mb);

  return NULL;
}
```

这个线程函数主要实现的功能就是从vi拿数据，使用rga进行处理，再送给venc,
此处的rga与上一章中的rga模块不同，此处是直接调用rga的api，
上一章是调用rkmedia中的rga。当然，其本质也是调用rga的api

我们先来看rkmedia部分的代码,与链路相关的API主要是下面几个:
1. RK_MPI_SYS_GetMediaBuffer 从指定模块获取buffer
2. RK_MPI_SYS_SendMediaBuffer 将buffer送给某个模块
3. RK_MPI_MB_CreateImageBuffer 创建一个新的buffer
4. RK_MPI_MB_ReleaseBuffer 释放buffer

>PS: 获取的buffer一定要及时释放，否则会阻塞（代码卡死）

观察上述代码不难发现，rkmedia中的buffer实际上就是MEDIA_BUFFER这个结构体,
为了描述这个buffer的信息，又引入来MB_IMAGE_INFO_S这个结构体，
为了获取这个buffer的一些信息，又使用了下面这些API
- RK_MPI_MB_GetFD 获取文件描述符
- RK_MPI_MB_GetTimestamp 获取时间戳

正常来说，我们是没必要关注时间戳的，但是编码需要依赖时间信息，
因此送给venc的图像buffer需要包含时间戳信息，因此就使用下面的API来设置
- RK_MPI_MB_SetTimestamp 设置时间戳

同时，venc还需要告知它裁切信息，因此便有了下面这段代码:

```c
// line 120
VENC_RESOLUTION_PARAM_S stResolution;
stResolution.u32Width = crop_arg->target_width;
stResolution.u32Height = crop_arg->target_height;
stResolution.u32VirWidth = crop_arg->target_width;
stResolution.u32VirHeight = crop_arg->target_height;

RK_MPI_VENC_SetResolution(g_stVencChn.s32ChnId, stResolution);
```

>PS: 虽然我觉得没必要（MB_IMAGE_INFO_S也包含这些），但是没了这段好像就没法运行
> 当成固定用法吧QAQ

关于rga的部分，主要涉及下面几个api
1. wrapbuffer_fd 通过文件描述符创建rga的buffer
2. imcrop 图像裁切操作
3. imcheck 校验rga操作的输入参数是否合法

观察上面的代码，我们可以知道，rga进行处理的时候需要先做一个映射，
将rkmedia的buffer转化为rga使用时候的buffer

>PS: 映射不等于拷贝，修改用rga进行的修改会实时的反映到rkmedia的buffer上

此外，可能会对crop_arg这个变量的流程比较迷惑，查看第78行我们可以发现，
它其实就是这个线程函数的输入参数，即arg，只不过进行来类型转换。
而arg这个函数又从何而来呢，答案是第355行的pthread_create函数的第4个参数，
即demo_arg变量

### 3.3.2 GetVencBuffer 线程函数

代码段
```c
// line 45
static void *GetVencBuffer(void *arg) {
  printf("#Start %s thread, arg:%p\n", __func__, arg);
  // init rtsp
  rtsp_demo_handle rtsplive = NULL;
  rtsp_session_handle session;
  rtsplive = create_rtsp_demo(554);
  session = rtsp_new_session(rtsplive, "/live/main_stream");
  rtsp_set_video(session, RTSP_CODEC_ID_VIDEO_H264, NULL, 0);
  rtsp_sync_video_ts(session, rtsp_get_reltime(), rtsp_get_ntptime());

  MEDIA_BUFFER mb = NULL;
  while (!quit) {
    mb = RK_MPI_SYS_GetMediaBuffer(g_stVencChn.enModId, g_stVencChn.s32ChnId,
                                   -1);
    if (mb) {
      printf(
          "-Get Video Encoded packet():ptr:%p, fd:%d, size:%zu, mode:%d, time "
          "= %llu.\n",
          RK_MPI_MB_GetPtr(mb), RK_MPI_MB_GetFD(mb), RK_MPI_MB_GetSize(mb),
          RK_MPI_MB_GetModeID(mb), RK_MPI_MB_GetTimestamp(mb));
      rtsp_tx_video(session, RK_MPI_MB_GetPtr(mb), RK_MPI_MB_GetSize(mb),
                    RK_MPI_MB_GetTimestamp(mb));
      RK_MPI_MB_ReleaseBuffer(mb);
    }
    rtsp_do_event(rtsplive);
  }
  // release rtsp
  rtsp_del_demo(rtsplive);
  return NULL;
}
```

这个线程函数主要的功能是: 从venc获取编码后的数据，并用rtsp推流出去.

与rkmedia相关的API主要是:
1. RK_MPI_SYS_GetMediaBuffer 获取buffer
2. RK_MPI_MB_GetPtr 获取buffer数据的指针
3. RK_MPI_MB_GetFD 获取buffer文件描述符号
4. RK_MPI_MB_GetSize 获取buffer大小
5. RK_MPI_MB_GetModeID 获取buffer模式id
6. RK_MPI_MB_GetTimestamp 获取buffer时间戳
7. RK_MPI_MB_ReleaseBuffer 释放buffer

上述API中2较为重要，因为c语言中获取数据的方式主要是依靠指针,
而4 5基本只是打印信息用

这个线程函数还有一部分代码则是rtsp部分，这部分是使用的瑞星微自己提供的API，
其封装成了一个静态库 librtsp.a 对应头文件为rtsp_demo.h
存放目录为: example/librtsp/rtsp_demo.h 我们无需过多关注，直接使用这个例子的调用方式即可

rtsp初始化与销毁代码:
```c
//line 47
// init rtsp
rtsp_demo_handle rtsplive = NULL;
rtsp_session_handle session;
rtsplive = create_rtsp_demo(554);
session = rtsp_new_session(rtsplive, "/live/main_stream");
rtsp_set_video(session, RTSP_CODEC_ID_VIDEO_H264, NULL, 0);
rtsp_sync_video_ts(session, rtsp_get_reltime(), rtsp_get_ntptime());

...

// release rtsp
rtsp_del_demo(rtsplive);
```

将venc的数据rtsp推流出去的代码:
```c
// line 65
rtsp_tx_video(session, RK_MPI_MB_GetPtr(mb), RK_MPI_MB_GetSize(mb),
              RK_MPI_MB_GetTimestamp(mb));

...

rtsp_do_event(rtsplive);
```


















# 参考链接

[C命令行参数](https://www.runoob.com/cprogramming/c-command-line-arguments.html)

[C语言linux getopt_long()函数（命令行解析）](https://blog.csdn.net/Dontla/article/details/121610581)

