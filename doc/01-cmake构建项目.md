[返回README](../README.md)


本文主要学习在linux下构建c项目，在linux下没有类似VS这样的集成化环境，
需要自行选择编译器并进行编译和链接。单个.c可以通过命令行指令来进行编译,
但对于大型项目，通常会不止一个文件，使用命令行进行是不现实的，
需要引入一些智能的批处理工具来辅助编译，考虑到通用性和便利性，
本项目便采用cmake来构建项目


# 1. 一些基本概念


## 1.1 编译和链接

编译可以理解成"翻译"，就是将我们写的c代码翻译成机器能看懂的二进制形式，
而实现翻译功能的东西就叫编译器

链接就是一个"打包"的过程，链接就是将编译生成的二进制文件跟一些组件整合到一起，
使其称为一个能运行的程序


## 1.2 gcc

它是GNU Compiler Collection（就是GNU编译器套件），也可以简单认为是编译器，
它可以编译很多种编程语言（括C、C++、Objective-C、Fortran、Java等等）

下面是一个简单的案例:

假设我们有这么一个c文件，名为"hello.c"

```c
// hello.c
#include <stdio.h>
int main()
{
        printf("hello world\n");
        return 0;
}
```

那么我们可以在该文件同级目录使用如下指令将其编译为可执行文件

```shell
gcc hello.c -o hello
```

执行完上述命令后，便会在同级目录产生一个名为 "hello"的文件，
这个文件旧类似与windows下的exe文件，是可以直接执行的，执行方式如下:

```shell
./hello
```


## 1.3 交叉编译

现在的cpu有多种不同的架构，常见的有 x86(PC), arm(rv1126),riscv等

同一份c代码要想在不同架构的cpu上执行，其二进制文件也会有所不同。
在一种架构上编译另一种架构上运行的可执行文件，这一过程称为**交叉编译**

举个栗子: 

arm=英国; x86=中国 显然中国人交流用英语而英国人用English
而中国人想跟去英国跟英国人交流就得使用English


上面栗子中的国籍就对应cpu架构；交流的语言对应编译器；
想表达的意思对应c代码；中国人说English的过程对应交叉编译



## 1.4 make和makefile

1.2中的指令只能编译一个c文件这种简单项目，对于复杂项目就无能为力了
(也能编译，但太麻烦了)。

make可以看成是一个智能的批处理工具，而是用类似于批处理的方式来辅助编译和链接，
而makefile则是make的配置文件，make和makefile的关系类似gcc和.c的关系
 

最常见的复杂项目就是kernel，它在每个子目录下基本都有一个Makefile，
执行编译的时候也是调用make工具来进行

> PS: make本身不具备编译和链接功能，需要搭配编译器使用


## 1.5 cmake 和 CMakeLists.txt

cmake就是一个用来生成Makefile的工具，为什么使用它呢？主要有以下几个原因:

1. cmake具有跨平台特性（网上都这么说，但我没试过 =_=）
2. cmake更简单，能更方便的管理项目(Makefile至今不会QAQ)
3. 很多大型项目用此来编译，比如 Opencv

至于CMakeLists.txt文件，就是类似Makefile的存在，用于配置cmake



[返回目录](#0)

# 2. 良好的 C/C++ 项目结构

对于一个大型项目，良好的文件结构会显得更有条理，也便于编译脚本
(如Makefile或CMakeLists)的编写。

一般来说，项目至少要包含几个部分:

|组成部分|本项目对应文件(夹)|作用或说明|
|-------|--------|--------|
|构建系统|CMakeLists.txt|用于编译和链接|
|自述文件|README.md LICENSE CHANGELOG.md等|项目说明和协议|
|源代码  |src      |程序源码|
|头文件  |include  |c需要知道类和函数的声明才能使用，因此头文件是必须的|  
|运行库  |lib      |调用的第三方包，或者不开源的代码，一般会封装成库|

> PS: 本项目根目录下的include存放的是整个项目通用的头文件，
> 对于子模块.c文件对应的.h，则存放在那个子模块文件夹下


# 3. 初识cmake

cmake发展至今，已经是一个庞然大物，但我们不必完全搞懂，
只需要很少一部分就能编译大部分项目了，下面我们就谈谈它常用的几个命令



## 3.1 交叉工具链配置

一般来说，cmake都有个默认的编译器(即gcc),但我们是想在PC上编译arm端运行的工具，
因此需要配置交叉工具链

工具链的位置就是 toolchain，我们需要在顶层的CMakeLists.txt中进行配置

```CmakeLists.txt
# 工具链配置
set(CMAKE_SYSROOT ${CMAKE_SOURCE_DIR}/toolchain/arm-buildroot-linux-gnueabihf/sysroot)
set(CMAKE_C_COMPILER ${CMAKE_SOURCE_DIR}/toolchain/bin/arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER ${CMAKE_SOURCE_DIR}/toolchain/bin/arm-linux-gnueabihf-g++)
```

从上面的代码不难看出，其实我们就是设置了3个变量 CMAKE_SYSROOT,
CMAKE_C_COMPILER和CMAKE_CXX_COMPILER 
CMAKE_SOURCE_DIR则是一个默认变量，代表根目录CMakeLists.txt所在文件夹。
这三个变量分别代表了虚拟根目录，c编译器和c++编译器

从中，我们也可以看到cmake一个基本的命令，即set，它用于定义变量，
而定义后的变量则可以使用 ${}来调用


## 3.2 项目结构的管理

### 3.2.1 项目名称

下面的命令用于定义项目名称，最好只调用一次，多次调用可能出现未知bug

```CMakeLists.txt
project(my_media)
```

### 3.2.2 添加子文件夹

查看src文件夹会发现，每个文件夹中都有一个CMakeLists.txt文件,
其实整个项目是以树状图的形式存在的，每个文件夹就是一个节点。
我们希望每个CMakeLists.txt都只需要管理当前这个节点的即可,
但cmake是无法自己判断出来的，因此需要调用 add_subdirectory
函数来告诉它

举个栗子，在根目录下的CMakeLists.txt中，就有下面这行

```CMakeLists.txt
add_subdirectory(src)       # 源码文件夹
```
这行就是告诉cmake，还有一个子文件夹叫src，它会自行去寻找src
目录下的CMakeLists.txt并执行相应命令


### 3.2.3 配置输出路径

除了添加子文件夹以外，我们还需要将编译生成的可执行文件放到对应位置，
主要就是靠设置下面这三个变量 他们分别是: 
可执行文件路径，动态库路径和静态库路径

```CMakeLists.txt
# 输出路径设置
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/output)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib)
```

## 3.3 编译选项配置


对于gcc编译器，我们很多时候需要添加一些选项，下面一个栗子

```CMakeLists.txt
# 编译宏
add_definitions(-g -O0 -ggdb -gdwarf -funwind-tables -rdynamic)
add_definitions(-DRKAIQ)
```

- -g -ggdb 表示开启gdb调试
- -DRKAIQ 表示开启宏开关, 代码中的 #ifdef RKAIQ 会打开

而实现这些功能是使用add_definitions这个函数

## 3.4 头和库文件搜索路径

### 3.4.1 头文件搜索路径

多文件编程时，头文件往往不在源码所在目录，这时候就需要指定头文件的搜索路径，
让编译器去这些路径下寻找对应的头文件主要是include_directories命令

比如 src目录下的CMakeLists.txt就有指定isp相关头文件,即rkaiq
```CMakeLists.txt
        # aiq相关头文件
include_directories(${CMAKE_SYSROOT}/usr/include/rkaiq)
include_directories(${CMAKE_SYSROOT}/usr/include/rkaiq/algos)
include_directories(${CMAKE_SYSROOT}/usr/include/rkaiq/common)
include_directories(${CMAKE_SYSROOT}/usr/include/rkaiq/ipc_server)
include_directories(${CMAKE_SYSROOT}/usr/include/rkaiq/iq_parser)
include_directories(${CMAKE_SYSROOT}/usr/include/rkaiq/uAPI)
include_directories(${CMAKE_SYSROOT}/usr/include/rkaiq/xcore)
include_directories(${CMAKE_SYSROOT}/usr/include/rga)
```

>PS: 
> 1. 更精细化的管理，可了解target_include_directories 
> 2. 需要先使用该命令再添加子文件夹才会对子文件夹生效

### 3.4.2 库文件搜索路径

库文件命令与头文件类似，对应api为 link_directories


src目录下的CMakeLists.txt就有本项目库文件路径

```CMakeLists.txt
# 指定库文件
link_directories(${CMAKE_SOURCE_DIR}/lib)
```

### 3.4.3 sysroot的作用

在2.1中，我们配置了交叉编译工具链，它涉及了三个变量，
其中c和c++编译器的含义显而易见，而sysroot可能会令人疑惑

其实sysroot主要是搜索头文件和库文件时候使用，主要涉及的是下面这两个路径

- ${sysroot}/usr/include   系统默认的头文件
- ${sysroot}/usr/lib       系统默认的库文件

这部分编译器原本是没有的，需要利用瑞星微的sdk编译整个系统后才能获得。
我们也可以将需要的库和头文件放到上述两个目录来免于设置搜索路径，
但会污染sysroot，降低其通用性和项目结构，因此不建议这么干。

但是换个思路，有时我们的项目编译好了但放到板端无法运行，
很有可能就是缺库或库版本不匹配，这时我们就能去库目录将涉及的库放入板端

## 3.5 编译和链接

### 3.5.1 编译

主要是使用add_executable命令，将源码编译成可执行文件。
但很多时候不止一个源码，一个个输入比较麻烦，我们也可以使用aux_source_directory
命令来快速获取对应文件夹下面的源码

下面是src目录下CMakeLists.txt的编译部分的命令

``` CMakeLists.txt
# 编译可执行文件
aux_source_directory(. SRC_SOURCES)                     # 获取当前目录下所有源文件
add_executable(main ${SRC_SOURCES})                     # 编译源码
```

上面的代码是编译可执行文件，但有时候我们需要编译库(比如编译子模块),
那我们就可以使用add_library来编译成库

下面是src目录下CMakeLists.txt的编译部分的命令

``` CMakeLists.txt
# 编译库
aux_source_directory(. COMMON_SOURCES)
add_library(common STATIC ${COMMON_SOURCES})
```

值得注意的是，库有动态库和静态库之分，动态库是在执行使用，静态库则是在编译时使用
我们可以通过 STATIC和SHARED来控制编译成静态库还是静态库

>PS:库名与变量也是有一定关系的比如common 对应libcommon.a/libcommon.so

### 3.5.2 链接

链接指令为: target_link_libraries 

下面是src目录下CMakeLists.txt的链接部分的命令

``` CMakeLists.txt
# 链接库
add_dependencies(main common)
target_link_libraries(main easymedia common pthread rkaiq rtsp rga)
```

值得注意的是，链接指令前还有一个指令 add_dependencies 该指令用于指定依赖关系，
只有当common编译完成后才会开始链接main

>PS:库名与变量也是有一定关系的比如common 对应libcommon.a/libcommon.so


## 3.6 本章变量汇总

- CMAKE_SYSROOT  虚拟系统目录
- CMAKE_C_COMPILER  c编译器
- CMAKE_CXX_COMPILER c++编译器
- CMAKE_RUNTIME_OUTPUT_DIRECTORY   可执行文件输出路径
- CMAKE_LIBRARY_OUTPUT_DIRECTORY   动态库输出路径
- CMAKE_ARCHIVE_OUTPUT_DIRECTORY   静态库输出路径

## 3.7 本章API汇总

|命令|说明|案例|
|---|----|---|
|project         |设置项目名称   |project(my_media)|
|set             |显示设置变量   |set(SRCS src)|
|add_subdirectory|增加子目录    |add_subdirectory(src)|
|add_definitions |添加编译器选项 |add_definitions(-DRKAIQ)|
|include_directories|增加头文件搜索路径|include_directories(${CMAKE_SOURCE_DIR}/include)|
|link_directories|增加库文件搜索路径|link_directories(${CMAKE_SOURCE_DIR}/lib)|
|aux_source_directory|自动添加源码|aux_source_directory(. SRC_SOURCES)
|add_executable  |编译源码为可执行文件|add_executable(main ${SRC_SOURCES})|
|add_library     |编译源码为库|add_library(common STATIC ${COMMON_SOURCES})|
|target_link_libraries|链接库|target_link_libraries(main easymedia common pthread rkaiq rtsp rga)|
|add_dependencies|指定依赖关系|add_dependencies(main common)|


[返回目录](#0)

# 4. 小练习

在pratice/ch1文件夹下面是瑞星微官方的yolov5 demo 请将其编译成功

这部分源码原本路径: ${SDK_ROOT}/external/rknpu/rknn/rknn_api/examples/rknn_yolov5_demo

现在路径 pratice/ch1/rknn_api/examples/rknn_yolov5_demo

> PS:目前这个文件藏得有点深，能不能修改它的CMakeLists.txt脚本，
> 移动几个文件位置，来精简其路径，最好让其路径变为:
> pratice/ch1/rknn_yolov5_demo

# 参考资料

[什么是编译和链接（通俗易懂，深入本质）](https://blog.csdn.net/qq_64844180/article/details/124237153)

[5分钟理解make/makefile/cmake/nmake](https://zhuanlan.zhihu.com/p/111110992)



[返回README](../README.md)
