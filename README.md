# 1. 项目简介
本项目用于学习rv1126平台的rkmedia框架，用git进行管理，
为了跟进更新，建议使用git来获取版本

对于windows系统，可按[该教程](https://zhuanlan.zhihu.com/p/242540359)
安装并使用git;对于ubuntu系统，可使用下面指令安装

``` shell
sudo apt install git
```

**拷贝本仓库:**

```shell
git clone https://gitee.com/jackyyellow/rkmedia.git
```

**更新本仓库**

```shell
git pull origin/master
```

>PS: 更新前可能需要清空所有修改，更直接的方式就是删除仓库重新克隆(=^=)

**切换章节**

本项目章节都有一个标签，可以通过下面的命令查看并切换到对应章节

``` shell
# 查看所有标签
git tag
# 切换到对应章节
git checkout ch1
```

# 2. 目录说明

- build            存放编译的临时文件 不放入git
- doc              存放相关文档
- examples         参考demo 获取路径 ${SDK_ROOT}/external/rkmedia/example
- include          项目相关头文件
- lib              项目相关库文件
- src              项目源码
- output           输出文件存放目录 不放入git
- partice          小练习，下章揭晓答案
- toolchain        交叉编译工具链，需要自己创建并下载
- CMakeLists.txt   cmake配置脚本

# 2.1交叉编译工具链获取方式:

主要有两种获取方式:

**1.通过在sdk上面编译buildroot后获取**

编译指令:
``` shell
cd ${SDK_ROOT}       # 进入sdk根目录
./build.sh lunch     # 选择合适的板极配置
./build.sh buildroot # 编译buildroot
```

编译后toolchain存放目录为: 

        ${SDK_ROOT}/buildroot/output/xxxxxxx/host

**2.直接使用正点原子rv1126的工具链**

[网盘链接](https://pan.baidu.com/s/12spzXODe12bqS-ji2nzijg?pwd=mt2y)

工具链位于

05.开发工具/01.交叉编译工具/atk-dlrv1126-toolchain-arm-buildroot-linux-gnueabihf-x86_64-20221205-v1.0.1.run

执行完后，会安装在/opt/atk-dlrv1126-toolchain 目录下

toolchain目录结构如下:

```shell
cd toolchain && tree . -d -L 1
.
├── arm-buildroot-linux-gnueabihf # sysroot在里面
├── bin
├── etc
├── include
├── lib
├── lib64 -> lib
├── libexec
├── man
├── mkspecs
├── sbin
├── share
├── usr -> .
└── var
```

# 3. 编译运行

本项目基于cmake进行构建，因此需要预先安装cmake

```shell
sudo apt install cmake
```

为防止cmake和编译生成的临时文件污染项目，需要在build文件夹下进行配置和编译

```shell
cd build
cmake ..
make -j12
```

生成的文件在install文件夹下，将其放入板端即可运行

``` shell
# adb连接的使用下面命令部署
adb push install /root/
# ssh连接的使用下面命令部署  ip根据需要自行修改
scp -r install 192.168.20.123:/root/
```

其本质是rkmedia_vi_venc_rtsp，但改名为main,运行指令为

```shell
./main -w 1920 -h 1080 -a /etc/iqfiles
```

# 4. 文档目录索引

[01-cmake构建项目.md](doc/01-cmake构建项目.md)<br>
[02-rkmedia简介.md](doc/02-rkmedia简介.md)<br>