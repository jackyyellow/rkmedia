#!/bin/bash

CUR_DIR=$(cd $(dirname $0);pwd)

function func_clean(){
    cd "$CUR_DIR" || exit
    rm build -rf
    rm lib/*.a
    rm install -rf
}

function func_build(){
    cd "$CUR_DIR" || exit
    mkdir -p build && \
    cd build && \
    cmake .. && \
    make -j12 && \
    cd ..
}

function func_install(){
    cd "$CUR_DIR" || exit
    cp model install/ -rf
    cp output/rknn_yolov5_demo install/
    adb push install /root/
}

function func_run(){
    adb shell "cd /root && ./rknn_yolov5_demo model/yolov5s.rknn test2.bmp"
}

function func_all(){
    func_clean
    func_build
    func_install
    func_run
}

function func_apply(){
    cd "$CUR_DIR" || exit
    cd build && \
    make -j12 && \
    cd ..
    func_install
    func_run
}

function print_usage(){
    echo "使用说明: build.sh [选项]"
    echo "选项       说明"
    echo "clean      清空已编译内容"
    echo "build      编译可执行文件"
    echo "install    将编译好的文件和model文件放入板端"
    echo "run        运行板端的可执行文件"
    echo "all        执行上述全部"
    echo "apply      编译并在板端运行,用于更改代码后快速看效果"

}

case $1 in
    clean) func_clean ;;
    build) func_build ;;
    install) func_install ;;
    run) func_run ;;
    all) func_all ;;
    *) print_usage;;
esac
