# 1. 编译指令

``` shell
mkdir -p build      # 创建build文件夹
cd build            # 进入build文件夹
cmake ..            # 使用cmake生成makefile
make -j12           # 编译
```

# 2. 制作install目录

本项目仅仅编译出可执行文件不够，还需将模型文件和图片存放到板端才可运行。
因此，可以使用下面指令来拷贝对应文件到install目录，最后将install目录整个放进板端即可

```shell
cd ${project_root}
mkdir -p install
cp model install/ -rf   # 拷贝模型文件和测试图片
cp output/rknn_yolov5_demo install/  # 拷贝可执行文件
```

# 3. 放入板端并执行

使用如下指令放到板端 /root目录
```shell
adb push install /root/
```

使用如下指令执行

./rknn_yolov5_demo model/yolov5s.rknn test2.bmp


# 4. build.sh脚本

上面的指令较为繁琐，故整理了一个build.sh脚本

使用说明如下:

```shell
#./build.sh --help

```

>PS: 使用该脚本需要确保能adb连接上板子