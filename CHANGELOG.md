日期: 2024-05-10<br>
作者: jackyyellow<br>
commit id: db2d216b3acd368c499d87566d3ec983d25cdfc7<br>
说明: 初始版本，rkmedia_vi_venc_rtsp.c案例编译通过<br>

日期: 2024-05-10<br>
作者: jackyyellow<br>
commit id: 60d4ec9df9a7996bef087628648002a3f949dc09<br>
说明:<br>
1. 新增第一章文档,位于doc目录
2. 新增pratice文件夹，用于存放每次的练习
2. 完善README.md，增加git部分操作，和toolchain目录结构
3. 完善src目录下CMakeList.txt 删除部分冗余命令


日期: 2024-05-21<br>
作者: jackyyellow<br>
commit id:aa0dd420e3b6d5ee44517830893476b83d28bd93<br>
说明: 
1. 在cmake脚本中添加-fPIC编译参数，修复编译报错问题
2. 删除文档中目录相关代码，因为gitee会自动生成目录
3. 新增第二章文档第一部分，简单介绍基础知识
4. 新增rga和rkmedia的参考文档，存放目录为/doc/ref

日期: 2024-05-21<br>
作者: jackyyellow<br>
commit id: 664a534693044c7346ce983557768c9e806569ce<br>
说明: 
1. 在README.md中增加第二章文档索引
2. 针对amd部分cpu无法编译问题，在src目录下CMakeLists.txt添加编译选项"-no-pie"
3. 给出ch1小练习参考，存放目录为pratice/ref/ch1/rknn_yolov5_demo

日期: 2024-05-24<br>
作者: jackyyellow<br>
commit id: <br>
说明: 完善第二章文档，增加对rkmedia_vi_vo_test.c和rkmedia_rga_crop_venc_test.c的代码讲解